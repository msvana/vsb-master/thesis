import os
from typing import *

import numpy
import pandas
from sklearn import model_selection
from sklearn import pipeline
from sklearn import preprocessing

import src.graph
import src.gridsearch
import src.stemmer

APP_DIR = os.path.dirname(__file__)
DATA_DIR = '/storage/datasets'
OUTPUT_DIR = '/artifacts'
NRSR_DATA_FILE = os.path.join(DATA_DIR, 'nrsr.json')

MIN_SAMPLE_AMOUNT = 250
MAX_SAMPLE_AMOUNT = 50000
MIN_SPEECH_LENGTH = 250
MAX_SPEAKERS = 10
GS_CONFIG = 'graph_svc'


def train():
    X_train, X_test, y_train, y_test = prepare_data()

    gs_config = src.gridsearch.GRID_SEARCH[GS_CONFIG]
    cls = pipeline.Pipeline(gs_config['steps'])
    gs = model_selection.GridSearchCV(
        cls, gs_config['params'], 'accuracy', verbose=2, cv=5, return_train_score=True, n_jobs=-1)
    gs.fit(X_train, y_train)

    print('Best validation score: %.4f' % gs.best_score_)
    score = gs.best_estimator_.score(X_test, y_test)
    print('Best classifier test score is: %.4f' % score)

    gs.cv_results_.pop('params')
    results_df = pandas.DataFrame(data=gs.cv_results_)
    results_df.to_csv(os.path.join(OUTPUT_DIR, '%s.csv' % GS_CONFIG), index=False)


def save_similarities():
    X_train, X_test, y_train, y_test = prepare_data()
    transformer = pipeline.Pipeline(src.gridsearch.GRID_SEARCH['graph']['steps'])
    X_train_transformed = transformer.fit_transform(X_train, y_train)
    X_test_transformed = transformer.transform(X_test)
    pandas.DataFrame(X_train_transformed).to_csv(os.path.join(OUTPUT_DIR, 'X_train.csv'), index=False)
    pandas.DataFrame(X_test_transformed).to_csv(os.path.join(OUTPUT_DIR, 'X_test.csv'), index=False)
    pandas.Series(y_train).to_csv(os.path.join(OUTPUT_DIR, 'y_train.csv'), index=False)
    pandas.Series(y_test).to_csv(os.path.join(OUTPUT_DIR, 'y_test.csv'), index=False)


def prepare_data():
    nrsr_df = pandas.read_json(NRSR_DATA_FILE, orient='records')
    X, y = nrsr_df['speech'].values, nrsr_df['speaker'].values
    X, y = filter_short_speeches(X, y, MIN_SPEECH_LENGTH)
    X, y = filter_by_sample_amount(X, y, MIN_SAMPLE_AMOUNT, MAX_SAMPLE_AMOUNT)

    unique_speakers = numpy.unique(y)
    unique_speakers_filtered = unique_speakers[:MAX_SPEAKERS]
    X = X[numpy.isin(y, unique_speakers_filtered)]
    y = y[numpy.isin(y, unique_speakers_filtered)]

    _, count = numpy.unique(y, return_counts=True)
    print('Total speakers:', len(count))
    print('Speakers counts: ', count)

    label_encoder = preprocessing.LabelEncoder()
    y = label_encoder.fit_transform(y)

    X_train, X_test, y_train, y_test = model_selection.train_test_split(
        X, y, stratify=y, test_size=0.1)
    return X_train, X_test, y_train, y_test


def filter_short_speeches(X: numpy.ndarray, y: numpy.ndarray, min_length: int) -> Tuple[numpy.ndarray, numpy.ndarray]:
    numpy_len = numpy.vectorize(len)
    have_min_len = numpy_len(X) >= min_length
    X = X[have_min_len]
    y = y[have_min_len]
    return X, y


def filter_by_sample_amount(
        X: numpy.ndarray, y: numpy.ndarray,
        min_amount: int, max_amount: int) -> Tuple[numpy.ndarray, numpy.ndarray]:
    _, idx, count = numpy.unique(y, return_inverse=True, return_counts=True)
    X = X[numpy.in1d(idx, numpy.where((max_amount >= count) & (count >= min_amount))[0])]
    y = y[numpy.in1d(idx, numpy.where((max_amount >= count) & (count >= min_amount))[0])]
    return X, y
