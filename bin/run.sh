#!/usr/bin/env bash
paperspace-python run --machineType C7 --isPreemptible false --workspace . --ignoreFiles ".idea/,.git" --command "python3 bin/train.py"